import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Col, Row, } from "react-materialize";
import Divider from "react-materialize/lib/Divider";

export default class Nav extends Component {
  render() {
    const bar = {
      position: "fixed",
      bottom: "0",
      width: "100%",
      marginBottom: "0",
      paddingTop: "2rem"
    };
    const barItem = {
      color: "#f2f2f2",
      textAlign: "center",
      marginTop: "4.5vh",
      textDecoration: "none"
    };
    return (
      <footer className="white">
          <Row style={bar} s={12} m={12} l={12} className="center white">
            <Col s={3} m={3} l={3}>
              <Link className="green-text" style={barItem} to="/">
                <i className="material-icons">home</i>
                <p>
                  <strong>Home</strong>
                </p>
              </Link>
            </Col>
            <Col s={3} m={3} l={3}>
              <Link className="green-text" style={barItem} to="/resume">
                <i className="material-icons">insert_drive_file</i>
                <p>
                  <strong>Resume</strong>
                </p>
              </Link>
            </Col>
            <Col s={3} m={3} l={3}>
              <Link className="green-text" style={barItem} to="/notes">
                <i className="material-icons">notes</i>
                <p>
                  <strong>Notes</strong>
                </p>
              </Link>
            </Col>
            <Col s={3} m={3} l={3}>
              <Link className="green-text" style={barItem} to="/links">
                <i className="material-icons">code</i>
                <p>
                  <strong>Links</strong>
                </p>
              </Link>
            </Col>
            <Divider />
          </Row>
      </footer>
    );
  }
}
