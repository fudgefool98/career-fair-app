import React, { Component } from "react";
import { Link } from "react-router-dom";
import Nav from "../components/nav";
import Title from "../components/title";
import { Table } from "react-materialize";
import fire from "../fire";
export default class Notes extends Component {
  constructor(props) {
    super(props);
    this.state = { companies: [], datas: [] }; // <- set up react state
  }
  componentWillMount() {
    this.getDateInputs();
  }
  getDateInputs = () => {
    let companiesRef = fire.database().ref("companyName/");
    companiesRef.on("child_added", snapshot => {
      let company = { text: snapshot.val(), id: snapshot.key };
      this.setState({ companies: [company].concat(this.state.companies) });
    });
  };
  alertBox = e => {
    if (
      window.confirm(
        "Would you like to delete the note for " + e.target.id + "?"
      )
    ) {
      //delete from db
      /* Send the message to Firebase */
      fire
        .database()
        .ref(`/companyName/${e.target.id}`)
        .set(null);
      fire
        .database()
        .ref(`/contactName/${e.target.id}`)
        .set(null);
      fire
        .database()
        .ref(`/contactNumber/${e.target.id}`)
        .set(null);
      fire
        .database()
        .ref(`/generalNotes/${e.target.id}`)
        .set(null);
      fire
        .database()
        .ref(`/opinion/${e.target.id}`)
        .set(null);
      fire
        .database()
        .ref(`/interview/${e.target.id}`)
        .set(null);
    }
  };
  render() {
    return (
      <div className="outer-container">
        <div className="container">
          <Title title="Notes" showButton={true} buttonLink="/new-note" />

          <Table hoverable={true}>
            <thead>
              <tr>
                <th className="center" data-field="id">
                  Company Name
                </th>
              </tr>
            </thead>
            <tbody>
              {this.state.companies.map(company => (
                <tr key={company.id} className="center">
                  <Link to="/edit-note">
                    <td onClick={this.props.onClick} key={company.id}>
                      {company.text}
                    </td>
                  </Link>
                  <i
                    id={company.text}
                    onClick={this.alertBox}
                    className="right material-icons"
                  >
                    delete
                  </i>
                </tr>
              ))}
            </tbody>
          </Table>
        </div>
        <Nav />
      </div>
    );
  }
}
