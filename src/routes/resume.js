import React, { Component } from "react";
import Nav from "../components/nav";
import Title from "../components/title";
import { Divider } from "react-materialize";
import "../resume.css";

export default class Resume extends Component {
  render() {
    return (
      <div className="outer-container">
        <div className="container">
          <Title title="Resume" />
          <h3 className="center-align">Jordan Liebman</h3>
          <p className="center-align">
            <strong>
              10 Port Way | Columbia, MO 65201 | (314) 341-9895 |
              jordanliebman329@gmail.com | https://github.com/fudgefool98
            </strong>
          </p>
          <h4>
            <i className="fas fa-user-tie blue-text" /> Technical and Related
            Experience
          </h4>
          <Divider />

          <div className="resume-list">
            <p>
              <strong>
                Instant Quote Website in ReactJS for Internship, Missouri
                Employers Mutual, Columbia MO <br />
                May 2018- August 2018
              </strong>
            </p>
            <ul className="browser-default">
              <li>
                Worked with React.js, to create custom components, and used
                imported components from NPM such as React-Router-DOM, and Axios
                JS HTTP client, and React-Materialize.
              </li>
              <li>
                Used custom .NET CORE APIs as well as industry API Valen for
                Insurance analytical data. Gained experience with an Agile team
                in a professional setting.
              </li>
              <li>
                Deployed builds to Docker containers to test mobile and desktop
                design requirements.
              </li>
            </ul>
            <br />
            <Divider />
            <p>
              <strong>
                Sales Commission Calculator UWP (Universal Windows Platform),
                Missouri Employers Mutual, Columbia MO
                <br />
                August 2018
              </strong>
            </p>

            <ul className="browser-default">
              <li>
                Developed an application that calculated Partnership Level,
                Commission rates and Commission totals for aid in selling
                policies.
              </li>
              <li>
                Deployed as a Universal Windows Platform (UWP) Application to
                Windows 10 devices
              </li>
              <li>Used HTML Javascript, JQuery, and Materialize CSS.</li>
            </ul>

            <Divider />
            <p>
              <strong>
                Website Fandomdb with a LAMP stack (Linux, Apache, MySql, PHP),
                Capstone Project <br />
                January 2018 - May 2018
              </strong>
            </p>
            <ul className="browser-default">
              <li>
                Lead a team of Senior Capstone IT Developers in the design and
                creation of a website and database.
              </li>
              <li>
                Created a relational database schema to hold users, their
                content, its fandom and allow for creation of new fandoms.
              </li>
              <li>
                Tied a Mysql PHP backend to a HTML and Bootstrap css front end,
                to dynamically populate pages with content created by users.
              </li>
              <li>
                Maintained the DNS and Development servers configuration for
                development and deployment.
              </li>
            </ul>
          </div>

          <h4>
            <i className="fas fa-graduation-cap blue-text" /> EDUCATION
          </h4>
          <Divider />

          <div className="resume-list">
            <p>
              <strong>
                University of Missouri Columbia Bachelor of Science
                <br />
                Expected Graduation Dec. 2018
              </strong>
            </p>
            <ul>
              <li>Major: Information Technology</li>
              <li>Minor: Computer Science</li>
              <li>Major GPA: 3.6</li>
            </ul>
            <h5>Completed Coursework Includes:</h5>
            <p>
              Capstone, WebDev(I,II), Object Oriented Programming(I,II), C#
              .NET, Database, Software Engineering, Algorithm Design(I,II),
              Cyber Security, Networking Technology, Statistics, Economics,
              Accounting, Entrepreneurship.
            </p>
            <Divider />
          </div>
          <h4>
            <i className="fas fa-briefcase blue-text" /> WORK EXPERIENCE
          </h4>
          <Divider />

          <div className="resume-list">
            <p>
              Regularly employed since 2007. Responsible, trusted member of
              several work teams.
            </p>
            <p>
              <strong>
                Enterprise Architect Part Time, Missouri Employers Mutual,
                Columbia Mo
                <br /> Aug 2018—Present
              </strong>
            </p>
            <p>
              Develop and maintain applications in NodeJS ReactJS and Windows
              Universal Platform Maintain ReactJS Quote Website
            </p>
            <p>
              <strong>
                Enterprise Architect Intern , Missouri Employers Mutual,
                Columbia Mo
                <br /> May 2018—Aug 2018
              </strong>
            </p>
            <p>
              Created ReactJS Quote Website, and presented our product to the
              CIO.
            </p>
            <p>
              <strong>
                User Support, Bonfyre, St. Louis MO
                <br /> Mar 2015—Aug 2015
              </strong>
            </p>
            <p>
              Supported users of this group-messaging app during customer
              events. Promoted the use of Bonfyre during conferences,
              conventions, etc.
            </p>
          </div>
          <h4>
            <i className="fas fa-clipboard-list blue-text" /> OTHER SKILLS &
            INFO
          </h4>
          <Divider />

          <div className="resume-list ">
            <ul className="browser-default">
              <li>
                Experience with Amazon Web Services, including API Gateway, S3
                buckets, Ec2 instances, and lambda functions
              </li>
              <li>
                Solid understanding of Agile principles, and Scrum framework
              </li>
              <li>
                Solid understanding of Windows, Linux, and Unix operating
                systems
              </li>
              <li>Confident public speaker</li>
            </ul>
          </div>
        </div>
        <Nav />
      </div>
    );
  }
}
