import React, { Component } from "react";
import Nav from "../components/nav";
import Title from "../components/title";
import { Button } from "react-materialize";
import fire from "../fire";

export default class NewNote extends Component {
  addCompany = e => {
    e.preventDefault(); // <- prevent form submit from reloading the page
    this.setFirebase();
    this.clear();
  };

  setFirebase = () => {
    /* Send the message to Firebase */
    fire
      .database()
      .ref(`/companyName/${this.inputEl1.value}`)
      .set(this.inputEl1.value);
    fire
      .database()
      .ref(`/contactName/${this.inputEl1.value}`)
      .set(this.inputEl2.value);
    fire
      .database()
      .ref(`/contactNumber/${this.inputEl1.value}`)
      .set(this.inputEl3.value);
    fire
      .database()
      .ref(`/generalNotes/${this.inputEl1.value}`)
      .set(this.inputEl4.value);
    fire
      .database()
      .ref(`/opinion/${this.inputEl1.value}`)
      .set(this.inputEl5.value);
    fire
      .database()
      .ref(`/interview/${this.inputEl1.value}`)
      .set(this.inputEl6.value);
  };

  clear = () => {
    this.inputEl1.value = "";
    this.inputEl2.value = "";
    this.inputEl3.value = "";
    this.inputEl4.value = "";
    this.inputEl5.value = "";
    this.inputEl6.value = "";
  };

  render() {
    return (
      <div className="outer-container">
        <div className="container">
          <Title title="New Note" />
          <form onSubmit={this.addCompany}>
            <input
              placeholder="Company Name"
              type="text"
              ref={el => (this.inputEl1 = el)}
            />
            <input
              placeholder="Contact Name"
              type="text"
              ref={el => (this.inputEl2 = el)}
            />
            <input
              placeholder="Contact Number"
              type="text"
              ref={el => (this.inputEl3 = el)}
            />
            <input
              placeholder="General Notes"
              type="text"
              ref={el => (this.inputEl4 = el)}
            />
            <input
              placeholder="Opinion"
              type="tel"
              ref={el => (this.inputEl5 = el)}
            />
            <input
              placeholder="Interview"
              type="text"
              ref={el => (this.inputEl6 = el)}
            />
            <Button>Submit</Button>
          </form>
        </div>
        <Nav />
      </div>
    );
  }
}
