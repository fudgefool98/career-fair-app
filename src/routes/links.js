import React, { Component } from "react";
import Nav from "../components/nav";
import Title from "../components/title";
import { Row, Col, Divider } from "react-materialize";

export default class Links extends Component {
  render() {
    return (
      <div className="outer-container">
        <div className="container">
          <Title title="Links" />
          <Row>
            <Col l={2}>
              <i class="fab fa-linkedin fa-4x blue-text center center-align" />
            </Col>
            <Col l={8}>
              <h5>
                <a
                  className="black-text textColor"
                  href="https://www.linkedin.com/in/jordan-liebman/"
                >
                  https://www.linkedin.com/in/jordan-liebman/
                </a>
              </h5>
            </Col>
          </Row>
          <Divider />
          <br />
          <Row>
            <Col l={2}>
              <i class="fab fa-gitlab fa-4x blue-text center center-align" />
            </Col>
            <Col l={8}>
              <h5>
                <a
                  className="black-text textColor"
                  href="https://gitlab.com/jlieb"
                >
                  https://gitlab.com/jlieb
                </a>
              </h5>
            </Col>
          </Row>
          <Divider />
          <br />
          <Row>
            <Col l={2}>
              <i class="fab fa-github-alt fa-4x blue-text center center-align" />
            </Col>
            <Col l={8}>
              <h5>
                <a
                  className="black-text textColor"
                  href="https://github.com/jordan329"
                >
                  https://github.com/jordan329
                </a>
              </h5>
            </Col>
          </Row>
          <Divider />
          <br />
          <Row>
            <Col l={2}>
              <i class="fab fa-github-alt fa-4x blue-text center center-align" />
            </Col>
            <Col l={8}>
              <h5>
                <a
                  className="black-text textColor"
                  href="https://github.com/fudgefool98"
                >
                  https://github.com/fudgefool98
                </a>
              </h5>
            </Col>
          </Row>
        </div>
        <Nav />
      </div>
    );
  }
}
