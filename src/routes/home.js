import React, { Component } from "react";
import Nav from "../components/nav";
import Title from "../components/title";
import { Row, Col, Divider } from "react-materialize";

export default class Home extends Component {
  render() {
    const weightName = {
      fontWeight: 250
    };
    const weightLocation = {};
    return (
      <div className="outer-container">
        <div className="container">
          <Title title="Welcome!" />
          <Row>
            <Col l={1} className="" />
            <Col l={10} className="">
              <Row>
                <Col l={2} />
                <Col l={10}>
                  <h3 style={weightName}>My name is Jordan,</h3>
                </Col>
              </Row>
              <Divider />
              <br />
              <Row>
                <Col l={2}>
                  <i className="fab fa-react fa-4x blue-text center center-align" />
                </Col>
                <Col l={8}>
                  <h5>
                    I am an aspiring Software Engineer/Web Developer, most
                    comfortable with Javascript and React.js, but willing to
                    learn and loving every moment of it!
                  </h5>
                  <p>
                    I have experience with C#, Java, Swift, PHP, SQL, and C from
                    course work, and have played around with python and kotlin
                    as well
                  </p>
                </Col>
              </Row>
              <Divider />
              <br />
              <Row>
                <Col l={2}>
                  <i className="fas fa-user-graduate fa-4x blue-text center center-align" />
                </Col>
                <Col l={8}>
                  <h5>
                    University of Missouri Columbia. (December '18)
                    <br />
                    B.S. in Information Technology, and a minor in Computer
                    Science.
                  </h5>
                </Col>
              </Row>
              <Divider />
              <br />
              <Row>
                <Col l={2}>
                  <i className="fas fa-plane fa-4x blue-text center center-align" />
                </Col>
                <Col l={8}>
                  <h4 style={weightLocation}>Moving to Denver!</h4>
                </Col>
              </Row>
            </Col>
            <Col l={1} />
          </Row>
        </div>
        <Nav />
      </div>
    );
  }
}
