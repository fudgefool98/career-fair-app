import React, { Component } from "react";
import Nav from "../components/nav";
import Title from "../components/title";
import { Button, Row, Col } from "react-materialize";
import fire from "../fire";
import { Redirect } from "react-router-dom";

export default class EditNote extends Component {
  constructor(props) {
    super(props);
    this.state = { companies: [] }; // <- set up react state
  }

  componentWillMount() {
    this.getFirebase("contactName");
    this.getFirebase("contactNumber");
    this.getFirebase("generalNotes");
    this.getFirebase("interview");
    this.getFirebase("opinion");
  }

  getFirebase = x => {
    let companiesRef = fire.database().ref(`${x}/${this.props.companyName}`);
    companiesRef.on("value", snapshot => {
      // console.log(snapshot.val(),snapshot.key);
      this.setState({ [x]: snapshot.val() });
    });
  };

  addCompany = e => {
    e.preventDefault(); // <- prevent form submit from reloading the page
    this.setFirebase();
    this.clear();
  };
  setFirebase = () => {
    /* Send the message to Firebase */
    fire
      .database()
      .ref(`/companyName/${this.inputEl1.value}`)
      .set(this.inputEl1.value);
    fire
      .database()
      .ref(`/contactName/${this.inputEl1.value}`)
      .set(this.inputEl2.value);
    fire
      .database()
      .ref(`/contactNumber/${this.inputEl1.value}`)
      .set(this.inputEl3.value);
    fire
      .database()
      .ref(`/generalNotes/${this.inputEl1.value}`)
      .set(this.inputEl4.value);
    fire
      .database()
      .ref(`/opinion/${this.inputEl1.value}`)
      .set(this.inputEl5.value);
    fire
      .database()
      .ref(`/interview/${this.inputEl1.value}`)
      .set(this.inputEl6.value);
  };

  clear = () => {
    this.inputEl1.value = "";
    this.inputEl2.value = "";
    this.inputEl3.value = "";
    this.inputEl4.value = "";
    this.inputEl5.value = "";
    this.inputEl6.value = "";
  };

  componentWillUnmount() {
    this.props.reset();
  }

  render() {
    if (typeof this.props.companyName === "undefined") {
      return <Redirect to="notes" />;
    }
    return (
      <div className="outer-container">
        <div className="container">
          <Title title="Edit Note" />
          <form onSubmit={this.addCompany}>
            <input
              defaultValue={this.props.companyName}
              placeholder="Company Name"
              type="text"
              ref={el => (this.inputEl1 = el)}
            />
            <input
              defaultValue={this.state.contactName}
              placeholder="Contact Name"
              type="text"
              ref={el => (this.inputEl2 = el)}
            />
            <input
              defaultValue={this.state.contactNumber}
              placeholder="Contact Number"
              type="text"
              ref={el => (this.inputEl3 = el)}
            />
            <input
              defaultValue={this.state.generalNotes}
              placeholder="General Notes"
              type="text"
              ref={el => (this.inputEl4 = el)}
            />
            <input
              defaultValue={this.state.opinion}
              placeholder="Opinion"
              type="tel"
              ref={el => (this.inputEl5 = el)}
            />
            <input
              defaultValue={this.state.interview}
              placeholder="Interview"
              type="text"
              ref={el => (this.inputEl6 = el)}
            />
            <Row>
              <Col className="center" s={6}>
                <Button>Submit</Button>
              </Col>
              <Col className="center" s={6}>
                <Button onClick={this.props.reset}>Reset</Button>
              </Col>
            </Row>
          </form>
          <br />
        </div>
        <Nav />
      </div>
    );
  }
}
