import React, { Component } from "react";
import "./App.css";
//routes
import Home from "./routes/home";
import Resume from "./routes/resume";
import Notes from "./routes/notes";
import NewNote from "./routes/new-note";
import EditNote from "./routes/edit-note";
// components
import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Links from "./routes/links";

class App extends Component {
  state = {
    fields: {}
  };

  onClick = e => {
    this.setState({ currentCompanyName: e.target.innerText });
  };
  reset = () => {
    this.setState({});
  };
  render() {
    const homeWp = () => {
      return <Home />;
    };
    const resumeWp = () => {
      return <Resume />;
    };
    const notesWp = () => {
      return <Notes onClick={this.onClick} />;
    };
    const linksWp = () => {
      return <Links />;
    };
    const newNoteWp = () => {
      return <NewNote />;
    };
    const editNoteWp = () => {
      return (
        <EditNote
          companyName={this.state.currentCompanyName}
          reset={this.reset}
        />
      );
    };

    return (
      <div className="App">
        <Router>
          <Switch>
            <Route exact path="/" render={homeWp} />
            <Route exact path="/resume" render={resumeWp} />
            <Route exact path="/notes" render={notesWp} />
            <Route exact path="/new-note" render={newNoteWp} />
            <Route exact path="/edit-note" render={editNoteWp} />
            <Route exact path="/links" render={linksWp} />
          </Switch>
        </Router>
        {/* {JSON.stringify(this.state, null, 2)} */}
      </div>
    );
  }
}

export default App;
